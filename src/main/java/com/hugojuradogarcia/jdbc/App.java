package com.hugojuradogarcia.jdbc;
/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) {
        StudentManager studentManager = new StudentManager();
        studentManager.findById(1);
        studentManager.findAllStudents();
        studentManager.findByLastNameStudents();
        studentManager.updateStudent(1);
        studentManager.deleteStudent(2);
    }
}
