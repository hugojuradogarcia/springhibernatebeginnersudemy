package com.hugojuradogarcia.jdbc;

import com.hugojuradogarcia.commons.HibernateUtils;
import com.hugojuradogarcia.entities.Student;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

public class StudentManager {

    public void saveStudent(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        try {
            System.out.println("Creating new student object ");
            Student student = new Student();

            student.setFirstName("Hugo");
            student.setLastName("Jurado");
            student.setEmail("hugojuradogarcia@gmail.com");

            session.beginTransaction();
            session.save(student);
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void findById(int id){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Student student = new Student();
        try {
            System.out.println("Find student by id ");

            session.beginTransaction();
            student = session.get(Student.class, id);

            System.out.println(student.toString());
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public List<Student> findAllStudents(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Student> students = new ArrayList<Student>();
        try {
            System.out.println("Find all students ");

            session.beginTransaction();
            students = session.createQuery("from Student").getResultList();

            for (Student student: students
                 ) {
                System.out.println(student.toString());
            }

            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return students;
    }

    public List<Student> findByLastNameStudents(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Student> students = new ArrayList<Student>();
        try {
            System.out.println("Find by Last Name students ");

            session.beginTransaction();
            students = session.createQuery("from Student s where s.lastName = 'Jalpa'").getResultList();

            for (Student student: students
                    ) {
                System.out.println(student.toString());
            }
            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return students;
    }

    public void updateStudent(int studentId){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Student student = new Student();
        try {
            System.out.println("Update student");

            session.beginTransaction();
            student = session.get(Student.class, studentId);
            student.setFirstName("Leonardo");

            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void deleteStudent(int studentId){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Student student = new Student();
        try {
            System.out.println("Delete student");

            session.beginTransaction();
            student = session.get(Student.class, studentId);
            session.delete(student);

            System.out.println("Delete student id = 2");
            session.createQuery("delete from Student where id = 1").executeUpdate();
            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
